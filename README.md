# shopkeeper-opendoc

## 介绍

聚合呗开放平台接口相关文档 

- [配送OpenApi doc](https://gitee.com/jhbopenapi/juhebei-opendoc/tree/master/delivery)
- [打印OpenApi doc](https://gitee.com/jhbopenapi/juhebei-opendoc/tree/master/printer)
- [打印OpenApi doc - English](https://gitee.com/jhbopenapi/juhebei-opendoc/raw/master/printer/README-en.pdf)
- [云音箱OpenApi doc](https://gitee.com/jhbopenapi/juhebei-opendoc/tree/master/spoker)

## 快速开始

1. 如何获取appid和密钥？请点击此处注册[大趋智能云打印机开放平台](https://open.trenditiot.com/)，之前已通过小助手注册的账号，可以使用注册时提供的手机号直接登录使用；

2. 阅读[接口调用协议约定](#接口调用协议约定)，熟悉签名规则，在http header准备好相关通用参数，也可以参考签名样例：[JAVA](https://gitee.com/jhbopenapi/printer-openapi-demo) [PHP](https://gitee.com/jhbopenapi/juhebei-opendoc/blob/master/demo/PHP%E7%AD%BE%E5%90%8D%E4%B8%8E%E8%AF%B7%E6%B1%82Demo.md)；

3. 完成上面的步骤后，就可以按需对接其它接口了；

**_PS: 对JSON不熟悉的同学，建议先使用Postman, fiddler之类的http请求工具先调通，帮助理清传参规则_**

---

## 接口调用协议约定

1. 协议：`https`;

2. 所有请求方法都用 `POST`;

3. 请求与响应内容都是json;

4. **Http header**通用固定传参：
   
   | Http Header参数 | 类型     | 备注                                                                             |
   | ------------- | ------ | ------------------------------------------------------------------------------ |
   | Content-Type  | string | 固定为 `application/json;charset=UTF-8`                                           |
   | appid         | string | 应用ID                                                                           |
   | uid           | string | 请求唯一ID，只能用来请求一次，不可以重复（例如: Java可以使用UUID，其它没有UUID实现的语言，可以采用时间戳+随机串的方式生成，保证不重复即可） |
   | stime         | long   | timestamp，请求时间戳，精确秒(UTC+8)                                                     |
   | sign          | string | 签名 md5(uid+appid+stime+appsecrect+请求Json内容)                                    |

**示例**：

```javascript
 POST https://printer.juhesaas.com/openapi/addPrinter
 Content-Type: application/json
 uid: {uid}
 appid: {appid}
 stime: 1617332013      # timestamp（秒）
 sign: {sign}           # 签名算法 md5(uid+appid+stime+appsecrect+请求Json内容)

 {你的http请求json body在这里}
```

5. 响应标准结构，包括平台响应外部请求与开发者应用对平台异步通知打印结果的响应

```json
  {
    "code": 0,            # 响应码，成功时响应 0；异常时，API将返回相应错误码
    "message":"ok",  # 响应消息，成功时响应 "ok"；异常时返回错误消息
    "data":               # 可选，对于列表返回空[]，对于空对象不返回属性
  }
```


**_注意：收到回调后，只有在按响应标准json结构数据 `{"message":"ok","code":0}` code为0时才会认为回调成功，否则平台判断下推失败，会进行三次重试，第一次2秒，第二次4秒，第三次10秒，三次没有接收到正确响应平台停止推送_**


---

## 异步消息回调

#### 回调业务类型：

| 业务类型 | 备注                                                   |
|------|------------------------------------------------------|
| 0    | 回调地址合法性校验事件，收到之后，按标准结构响应：`{"code":0,"message":"ok"}` |
| 4    | 配送单状态变更事件                                            |
| 5    | 打票请求状态变化消息                                           |



响应示例：

```json
{
    "type": 4, // 见附录回调业务类型
    "rtime": 1662459209, // 回调时间
    "data": "{\"shopId\":6308559984,\"salesOrderNo\":\"A00062\",\"status\":60,\"deliverymanName\":\"甜甜田超辉\",\"deliverymanMobile\":\"18521736087\",\"distance\":1,\"deliveryAmount\":100,\"couponAmount\":0,\"tipsAmount\":300,\"insuranceAmount\":0,\"payAmount\":100,\"deductAmount\":0,\"timestamp\":1637720013}"
}
```
