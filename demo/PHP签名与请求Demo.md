- 替换` *****`为您自己真实信息

```php
<?php

class PrinterExample
{
    /**
     * $appId和$appSecret 自行修改为开放平台下发的。
     */
    private $appId = "*******";
    private $appSecret = "******";

    /**
     * No1：添加打印设备
     */
    function addPrinter()
    {
        $url = "https://iot-device.trenditiot.com/openapi/addPrinter";
        $data = [
            [
                "sn" => '5700******',
                "key" => '*****',
                "name" => 'trendit-p7'
            ]
        ];
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No1.1：修改打印设备（名称）
     */
    function editPrinter()
    {
        $url = "https://iot-device.trenditiot.com/openapi/editPrinter";
        $data = [
            [
                "sn" => '5700******',
                "name" => 'p7-update'
            ]
        ];
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No1.2：删除打印设备（名称）
     */
    function delPrinter()
    {
        $url = "https://iot-device.trenditiot.com/openapi/delPrinter";
        $data = ["5700******"];
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No2：查询打印机状态
     */
    function deviceStatus()
    {
        $url = "https://iot-device.trenditiot.com/openapi/getDeviceStatus";
        $data = array("sn" => "5700******");
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No3：打印小票
     */
    function print()
    {
        $url = "https://iot-device.trenditiot.com/openapi/print";

        $time = time();
        $data = array(
            "sn" => "5700******",
            "voice" => 1,
            "voicePlayTimes" => 1,
            "voicePlayInterval" => 3,
            "content" => "<C>中英文HELLO,.$time</C> ",
            "copies" => 1);
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No4：播放收银语音
     */
    function voice()
    {
        $url = "https://iot-device.trenditiot.com/openapi/payInVoice";
        $data = array(
            "sn" => "5700******",
            "payChannel" => 2,
            "payAmount" => 2000,
            "voicePlayTimes" => 2,
            "voicePlayInterval" => 1
        );
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No5：查询小票打印状态
     */
    function printStatus()
    {
        // printId 取print()返回值
        $url = "https://iot-device.trenditiot.com/openapi/getPrintStatus";
        $data = array(
            "sn" => "5700******",
            "printId" => "1024747035720622081");
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No6：设置打印浓度
     */
    function setDensity()
    {
        // 注意：density值可选值为[4,5,6,7]
        $url = "https://iot-device.trenditiot.com/openapi/setDensity";
        $data = array(
            "sn" => "5700******",
            "density" => 4);
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No7：设置打印浓度
     */
    function setVolume()
    {
        // 注意：density值可选值为[1,2,3,4,5]
        $url = "https://iot-device.trenditiot.com/openapi/setVolume";
        $data = array(
            "sn" => "5700******",
            "volume" => 1);
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    /**
     * No8：清除打印机待处理队列信息
     */
    function cleanWaitingQueue()
    {
        // 注意：density值可选值为[1,2,3,4,5]
        $url = "https://iot-device.trenditiot.com/openapi/cleanWaitingQueue";
        $data = array("sn" => "5700******");
        $resp = $this->http($url, 'POST', $data);
        echo $resp[1];
    }

    function getHeader($data)
    {
        $uid = $this->uuidv4();
        $time = time();
        $json = json_encode($data);
        $str = $uid . $this->appId . $time . $this->appSecret . $json;
        $md5 = md5($str);
        $header = array();
        $header[] = "Content-Type:application/json";
        $header[] = "appid:" . $this->appId;
        $header[] = "uid:" . $uid;
        $header[] = "stime:" . $time;
        $header[] = "sign:" . $md5;

        return $header;
    }

    function http($url, $method = 'GET', $formfields = null)
    {
        $header = $this->getHeader($formfields);
        $body = json_encode($formfields);

        $ci = curl_init();
        curl_setopt($ci, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ci, CURLOPT_TIMEOUT, 30);
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ci, CURLINFO_HEADER_OUT, true);
        curl_setopt($ci, CURLOPT_HTTPHEADER, $header); // 设置通用传参

        switch ($method) {
            case 'POST':
                curl_setopt($ci, CURLOPT_POST, true);
                if (!empty($formfields)) {
                    curl_setopt($ci, CURLOPT_POSTFIELDS, $body);
                }
                break;
        }
        curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, false);// 从证书中检查SSL加密算法是否存在
        curl_setopt($ci, CURLOPT_URL, $url);
        $response = curl_exec($ci);
        $http_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);
        curl_close($ci);
        return array($http_code, $response);
    }

    function uuidv4()
    {
        return uniqid('rxid');
    }
}

$example = new PrinterExample();
$example->addPrinter();
echo "\n";
$example->editPrinter();
echo "\n";
$example->delPrinter();
echo "\n";
$example->deviceStatus();
echo "\n";
$example->print();
echo "\n";
$example->printStatus();
echo "\n";
$example->setDensity();
echo "\n";
$example->print();
echo "\n";
$example->setVolume();
echo "\n";
$example->voice();
echo "\n";
$example->cleanWaitingQueue();


```
